﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float _speed = 100f;

    void Start() { }

    void Update()
    {
        float vertical = Input.GetAxis("Vertical") * _speed;
        float horizontal = Input.GetAxis("Horizontal") * _speed;
        vertical *= Time.deltaTime;
        horizontal *= Time.deltaTime;
        transform.Translate(horizontal, vertical, 0);
    }
}
