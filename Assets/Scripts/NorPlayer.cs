﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NorPlayer : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        bool up = Input.GetKey(KeyCode.UpArrow);
        bool down = Input.GetKey(KeyCode.DownArrow);
        bool left = Input.GetKey(KeyCode.LeftArrow);
        bool right = Input.GetKey(KeyCode.RightArrow);

        if (up) {
            transform.position = transform.position + new Vector3(0, 200f, 0) * Time.deltaTime;
        }

        if (down) {
            transform.position = transform.position + new Vector3(0, -200f, 0) * Time.deltaTime;
        }

        if (left) {
            transform.position = transform.position + new Vector3(-200f, 0, 0) * Time.deltaTime;
        }

        if (right) {
            transform.position = transform.position + new Vector3(200f, 0, 0) * Time.deltaTime;
        }


        //bool up = Input.GetKey(KeyCode.UpArrow);
        //transform.position = transform.position + new Vector3(2f, 0, 0);
        //Time.deltaTime

    }
}
